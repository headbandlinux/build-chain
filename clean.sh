#!/bin/bash

# Delete $CONTAINER and start over.

buildah $BUILDAH_ARGS unmount $CONTAINER
buildah $BUILDAH_ARGS rm $CONTAINER
