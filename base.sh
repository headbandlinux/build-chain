#!/bin/bash

# If you need more than busybox, adjust this package list.
PACKAGES="busybox"

dnf install -y --installroot $CONTAINER_ROOT --releasever 27 $PACKAGES

# create basic rootfs directories
# Some of the linking is going to be fedora specific...
cd $CONTAINER_ROOT
mkdir {root,home,etc,opt,dev,proc,sys,media,mnt,tmp,run,srv}
mkdir usr/{bin,sbin,local,src,include,lib64} run/lock var/tmp
ln -s usr/bin bin
ln -s usr/sbin sbin
ln -s usr/lib lib
ln -s usr/lib64 lib64

cd $CONTAINER_ROOT/usr
ln -s ../var/tmp tmp

cd $CONTAINER_ROOT/var
ln -s ../run run
ln -s ..run/lock lock


# Create busybox links
cd $CONTAINER_ROOT/sbin
for i in `./busybox --list-full`; do
  cd $CONTAINER_ROOT
  ln -s /sbin/busybox $i
done
