#!/bin/bash


BIN="$1"
DIR="$2"

# This will only follow one link.
link_getter() {
  echo "copying $1"
  if readlink "$1" > /dev/null ; then
    cp -P --parents "$1" "$2"
    link_getter `realpath "$1"` $2
  else
    cp -P --parents "$1" "$2"
  fi
}

for lib in `ldd "$BIN" | cut -d'>' -f2 | awk '{print $1}'` ; do
   if [ -f "$lib" ] ; then
        link_getter "$lib" "$DIR"
   fi
done
