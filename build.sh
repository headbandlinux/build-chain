#!/bin/bash

PACKAGES="nginx"

# This will install the package to our build host, ensuring we get all the
# necessary dependencies.
dnf install -y $PACKAGES

# This will copy nginx + full path to our container root.
#cp -P --parents $(which nginx) $CONTAINER_ROOT
dnf download nginx -y
rpm2cpio nginx-*.rpm | cpio -dium -D $CONTAINER_ROOT

sh getlibs.sh $(which nginx) $CONTAINER_ROOT

# Need to copy over libnss bits
cp -P --parents `realpath /usr/lib64/libnss3.so` $CONTAINER_ROOT
cp -P --parents /usr/lib64/libnss_files.so.2 $CONTAINER_ROOT
cp -P --parents `realpath /usr/lib64/libnss_files.so.2` $CONTAINER_ROOT
sh getlibs.sh /usr/lib64/libnss3.so $CONTAINER_ROOT

ln -sf /dev/stdout $CONTAINER_ROOT/var/log/nginx/access.log
ln -sf /dev/stderr $CONTAINER_ROOT/var/log/nginx/error.log

dnf clean all --installroot $CONTAINER_ROOT --releasever 27
rm -Rf $CONTAINER_ROOT/var/cache/dnf
rm -Rf $CONTAINER_ROOT/var/lib/dnf
rm -Rf $CONTAINER_ROOT/var/lib/rpm

buildah $BUILDAH_ARGS config --cmd "/usr/sbin/nginx -g 'daemon off;'" $CONTAINER
buildah $BUILDAH_ARGS config --port 80/tcp $CONTAINER

buildah $BUILDAH_ARGS commit $CONTAINER docker-daemon:nginx-example1:latest
