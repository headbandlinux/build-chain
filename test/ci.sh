#!/bin/bash

set -x

tmp=`mktemp -d`

mkdir $tmp/root $tmp/runroot;
export BUILDAH_ARGS="-runroot $tmp/runroot -storage-driver vfs"

export CONTAINER=$(buildah $BUILDAH_ARGS from scratch)
export CONTAINER_ROOT=$(buildah $BUILDAH_ARGS mount $CONTAINER)


./base.sh

./build.sh
