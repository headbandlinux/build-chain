Building bare-minimum from scratch containers with buildah and Fedora.

# Why?
I don't care for Dockerfile syntax, I wanted something a little more portable.

## Why not from Fedora?
The Fedora image ships more than I need; I want to try to save bandwidth
and disk space.

## Why not something from Docker Hub?
While there are surely many great binaries available, I prefer something
I can create myself, and know exactly what's in it.

# Setup
You'll need a Fedora build host (I'm using F27 Workstation).

You'll need to install buildah:
```sh
dnf install -y buildah
```

Like most container builders, you'll need to perform the bulk of the steps
as root.

# Build base scratch image
Next, we need to create a from scratch container to use with buildah.

The following will create the scratch containers and export some env vars.

```sh
. start
```

Once the container root is mounted, we can start with a base file system and
busybox.

```sh
base.sh
```

You should now be able to use this install as a chroot:

```sh
chroot $CONTAINER_ROOT /bin/sh
```

# Install something useful

In this example, we're going to install nginx into the container.
